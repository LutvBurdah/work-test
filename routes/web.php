<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\RegisterController;

Route::get('/', function () {
    return view('dashboard.home');
});


Route::get('/barangMasuk', 'barangMasukController@index')->name('barangMasuk.index');
Route::get('/barangMasuk/create', 'barangMasukController@create')->name('barangMasuk.create');
Route::post('/barangMasuk/store', 'barangMasukController@store')->name('barangMasuk.store');
Route::get('/barangMasuk/edit/{id}', 'barangMasukController@edit')->name('barangMasuk.edit');
Route::put('/barangMasuk/upate/{id}', 'barangMasukController@update')->name('barangMasuk.update');
Route::delete('/barangMasuk/destroy/{id}', 'barangMasukController@destroy')->name('barangMasuk.destroy');

Route::get('/barangKeluar', 'barangKeluarController@index')->name('barangKeluar.index');
Route::get('/barangKeluar/create', 'barangKeluarController@create')->name('barangKeluar.create');
Route::post('/barangKeluar/store', 'barangKeluarController@store')->name('barangKeluar.store');
Route::get('/barangKeluar/edit/{id}', 'barangKeluarController@edit')->name('barangKeluar.edit');
Route::put('/barangKeluar/upate/{id}', 'barangKeluarController@update')->name('barangKeluar.update');
Route::delete('/barangKeluar/destroy/{id}', 'barangKeluarController@destroy')->name('barangKeluar.destroy');

Route::get('/transaksi', 'transaksiController@index')->name('transaksi.index');
Route::get('/transaksi/create', 'transaksiController@create')->name('transaksi.create');
Route::post('/transaksi/store', 'transaksiController@store')->name('transaksi.store');
Route::get('/transaksi/edit/{id}', 'transaksiController@edit')->name('transaksi.edit');
Route::put('/transaksi/upate/{id}', 'transaksiController@update')->name('transaksi.update');
Route::delete('/transaksi/destroy/{id}', 'transaksiController@destroy')->name('transaksi.destroy');

Route::get('/barang', 'barangController@index')->name('barang.index');
Route::get('/barang/create', 'barangController@create')->name('barang.create');
Route::post('/barang/store', 'barangController@store')->name('barang.store');
Route::get('/barang/edit/{id}', 'barangController@edit')->name('barang.edit');
Route::put('/barang/upate/{id}', 'barangController@update')->name('barang.update');
Route::delete('/barang/destroy/{id}', 'barangController@destroy')->name('barang.destroy');

Route::get('/category', 'categoryController@index')->name('category.index');
Route::get('/category/create', 'categoryController@create')->name('category.create');
Route::post('/category/store', 'categoryController@store')->name('category.store');
Route::get('/category/edit/{id}', 'categoryController@edit')->name('category.edit');
Route::put('/category/upate/{id}', 'categoryController@update')->name('category.update');
Route::delete('/category/destroy/{id}', 'categoryController@destroy')->name('category.destroy');

Route::get('/laporan/transaksi', 'laporanController@transaksiPDF')->name('transaksiPDF');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/register', [RegisterController::class, 'register'])->name('register');
