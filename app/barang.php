<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class barang extends Model
{
    protected $table = 'barang';

    protected $fillable = [
        'nama_barang', 'harga_barang', 'stock', 'category_id'
    ];

    public function barangMasuk()
    {
        return $this->hasMany(barangMasuk::class);
    }

    public function barangKeluar()
    {
        return $this->hasMany(barangKeluar::class);
    }

    public function transaksi()
    {
        return $this->hasMany(transaction::class);
    }
    public function category()
    {
        return $this->belongsTo(category::class);
    }
}
