<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class barangKeluar extends Model
{
    protected $table = 'barangKeluar';
    protected $fillable = [
        'barang_id', 'jumlah', 'tanggal_keluar'
    ];
    public function barang()
    {
        return $this->belongsTo(barang::class);
    }
}
