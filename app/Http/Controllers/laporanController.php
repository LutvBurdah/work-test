<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\transaction;
use Dompdf\Options;
use Dompdf\Dompdf;

class laporanController extends Controller
{
    public function transaksiPDF()
    {
        $transaction = transaction::all();
        $transaction = transaction::all();
        $html = view('transaksi.cetakLaporan', compact('transaction'))->render();
        $options = new Options();
        $options->set('isHtml5ParserEnabled', true);
        $dompdf = new Dompdf($options);
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();  
        return $dompdf->stream('transactions_report.pdf');
    }
}
