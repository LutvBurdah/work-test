<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\barangMasuk;
use App\barang;

class barangMasukController extends Controller
{
    public function index()
    {
        $barangMasuk = barangMasuk::all();
        $barang = barang::all();
        return view('barangMasuk.index', compact('barangMasuk', 'barang'));
    }

    public function create()
    {
        $barang = barang::all();
        return view('barangMasuk.create',compact('barang'));
    }

    public function store(Request $request)
{
    // Validasi input dari request
    $request->validate([
        'barang_id' => 'required|exists:barang,id',
        'jumlah' => 'required|integer',
        'tanggal_masuk' => 'required|date',
    ]);

    DB::transaction(function() use ($request) {
        // Buat data barang masuk baru
        $barangMasuk = barangMasuk::create($request->all());
        $barang = barang::find($request->barang_id);

        if ($barang) {
            $barang->stock += $request->jumlah;
            $barang->save();
        } else {
            DB::rollBack();
            return redirect()->route('barangMasuk.index')->with('error', 'Barang not found.');
        }
    });  

    return redirect()->route('barangMasuk.index')->with('success', 'Barang masuk created successfully.');
}


    public function show($id)
    {
        $barangMasuk = barangMasuk::findOrFail($id);
        return view('barangMasuk.show', compact('barangMasuk'));
    }

    public function edit($id)
    {
        $barangMasuk = barangMasuk::findOrFail($id);
        return view('barangMasuk.edit', compact('barangMasuk'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_barang' => 'required',
            'jumlah' => 'required|integer',
            'tanggal_masuk' => 'required|date',
        ]);

        $barangMasuk = barangMasuk::findOrFail($id);
        
        $barangMasuk->update($request->all());

        return redirect()->route('barangMasuk.index')->with('success', 'Barang masuk updated successfully.');
    }

    public function destroy($id)
    {
        $barangMasuk = barangMasuk::findOrFail($id);
        $barangMasuk->delete();

        return redirect()->route('barangMasuk.index')->with('success', 'Barang masuk deleted successfully.');
    }
}
