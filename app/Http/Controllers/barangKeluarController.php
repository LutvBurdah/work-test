<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\barang;
use App\barangKeluar;

class barangKeluarController extends Controller
{
    public function index()
    {
        $barang  = barang::all();
        $barangKeluar = barangKeluar::with('barang')->get();
        return view('barangKeluar.index', compact('barangKeluar', 'barang'));
    }

    public function create()
    {
        $barang = barang::all();
        return view('barangKeluar.create', compact('barang'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'barang_id' => 'required|exists:barang,id',
            'jumlah' => 'required|integer',
            'tanggal_keluar' => 'required|date',
        ]);

        DB::transaction(function () use ($request) {
            $barangKeluar = barangKeluar::create($request->all());  
            $barang = barang::findOrFail($request->barang_id);
            $barang->stock -= $request->jumlah;
            $barang->save();
        });

        return redirect()->route('barangKeluar.index')->with('success', 'Barang keluar created successfully.');
    }

    public function edit($id)
    {
        $barangKeluar = barangKeluar::findOrFail($id);
        $barang = barang::all();
        return view('barangKeluar.edit', compact('barangKeluar', 'barang'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'barang_id' => 'required|exists:barang,id',
            'jumlah' => 'required|integer',
            'tanggal_keluar' => 'required|date',
        ]);

        $barangKeluar = barangKeluar::findOrFail($id);
        $barang = barang::findOrFail($barangKeluar->barang_id);
        
        // Update stock in Barang table
        $barang->stock += $barangKeluar->jumlah; // revert old jumlah
        $barang->stock -= $request->jumlah; // subtract new jumlah

        $barangKeluar->update($request->all());
        $barang->save();

        return redirect()->route('barangKeluar.index')
                         ->with('success', 'Barang keluar updated successfully.');
    }

    public function destroy($id)
    {
        $barangKeluar = barangKeluar::findOrFail($id);
        $barang = barang::findOrFail($barangKeluar->barang_id);

        // Update stock in Barang table
        $barang->stock += $barangKeluar->jumlah;
        $barangKeluar->delete();
        $barang->save();

        return redirect()->route('barangKeluar.index')
                         ->with('success', 'Barang keluar deleted successfully.');
    }
}
