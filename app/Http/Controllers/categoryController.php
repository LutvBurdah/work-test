<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;

class categoryController extends Controller
{
    public function index()
    {
        $category = category::all();
        return view('category.index', compact('category'));
    }

    public function create()
    {
        $category = category::all();
        return view('category.create', compact('category'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'category_name' => 'required|string|max:255',
        ]);
        category::create($request->all());
        return redirect()->route('category.index')->with('suksess', 'category berhasil di Tambahkan.');
    }

    public function edit($id)
    {
        $category = category::find($id);
        return view('category.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $category = category::find($id);
        $category->name = $request->name;
        $category->save();
        return redirect()->route('category.index');
    }

    public function destroy($id)
    {
        $category = category::find($id);
        $category->delete();
        return redirect()->route('category.index');
    }
}
