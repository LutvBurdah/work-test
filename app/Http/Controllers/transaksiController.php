<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\transaction;
use App\barang;

class transaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksi = transaction::with('barang')->get();
        return view('transaksi.index', compact('transaksi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        {
            $barang = barang::all();
            return view('transaksi.create', compact('barang'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'barang_id' => 'required|exists:barang,id',
            'jenis_transaksi' => 'required|in:masuk,keluar',
            'jumlah_barang' => 'required|integer',
            'tanggal_transaksi' => 'required|date',
        ]);

        transaction::create($request->all());
        return redirect()->route('transaksi.index')->with('success', 'Transaksi created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        {
            $transaksi = transaction::findOrFail($id);
            $barang = barang::all();
            return view('transaksi.edit', compact('transaksi', 'barang'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'barang_id' => 'required|exists:barang,id',
            'jenis_transaksi' => 'required|in:masuk,keluar',
            'jumlah_barang' => 'required|integer',
            'tanggal_transaksi' => 'required|date',
        ]);

        $transaksi = transaction::findOrFail($id);
        $transaksi->update($request->all());

        return redirect()->route('transaksi.index')->with('success', 'Transaksi updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaksi = transaction::findOrFail($id);
        $transaksi->delete();

        return redirect()->route('transaksi.index')->with('success', 'Transaksi deleted successfully.');
    }
}
