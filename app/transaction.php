<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaction extends Model
{
    protected $table = 'transaksi';
    protected $fillable = [
        'barang_id', 'jenis_transaksi', 'jumlah_barang', 'tanggal_transaksi'
    ];
    public function barang()
    {
        return $this->belongsTo(barang::class);
    }
}
