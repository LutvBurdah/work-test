<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class barangMasuk extends Model
{
    protected $table = 'barangMasuk';
    protected $fillable = [
        'barang_id', 'jumlah', 'tanggal_masuk'
    ];
        public function barang()
    {
        return $this->belongsTo(barang::class);
    }
}
