<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laporan Transaksi</title>
    <style>
        /* Tambahkan gaya CSS sesuai kebutuhan Anda */
        table {
            width: 100%;
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
    <h2>Laporan Transaksi</h2>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Nama Barang</th>
                <th>Jenis Transaksi</th>
                <th>Jumlah Barang</th>
                <th>Tanggal Transaksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($transaction as $transaction)
            <tr>
                <td>{{ $transaction->id }}</td>
                <td>{{ $transaction->barang->nama_barang }}</td> 
                <td>{{ $transaction->jenis_transaksi }}</td>
                <td>{{ $transaction->jumlah_barang }}</td>
                <td>{{ $transaction->tanggal_transaksi }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    
</body>
</html>
