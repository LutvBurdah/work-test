@extends('partial.template')
@section('content')
<div class="table-responsive mt-3 p-5">
    <h1 class="text-center fw-bold">Transaksi</h1>
    <div class="mt-5">
        <a href="{{ route('transaksi.create') }}" class="btn btn-primary mt-2"><i class='bx bx-book-heart'></i> Transaksi</a>
        @if ($message = Session::get('success'))
            <p>{{ $message }}</p>
        @endif
    </div>
    <style>
        .table {
            border: 2px solid black;
        }

        .table th, .table td {
            border: 2px solid black; 
        }

        .table thead th {
            border-bottom: 3px solid black;
        }

        .btn-primary {
            margin-left: 5px; 
        }
    </style>
    <table class="table table-bordered mt-3 text-center">
        <tr>
            <th>ID</th>
            <th>Nama Barang</th>
            <th>Jenis Transaksi</th>
            <th>Jumlah</th>
            <th>Tanggal Transaksi</th>
            <th>Actions</th>
        </tr>
        @foreach ($transaksi as $tran)
            <tr>
                <td>{{ $tran->id }}</td>
                <td>{{ $tran->barang->nama_barang }}</td>
                <td>{{ $tran->jenis_transaksi }}</td>
                <td>{{ $tran->jumlah_barang }}</td>
                <td>{{ $tran->tanggal_transaksi }}</td>
                <td>
                    <a href="{{ route('transaksi.edit', $tran->id) }}" title="Edit" class="btn btn-success">
                        <i class='bx bx-pencil'></i>
                    </a>
                    <form action="{{ route('transaksi.destroy', $tran->id) }}" method="POST" style="display:inline">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger" title="Hapus">
                            <i class='bx bx-trash'></i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
</div>
@endsection


