@extends('partial.template')
@section('content')
    <div class="table-responsive mt-5 p-5">
        <div>
            <a href="{{ route('barangKeluar.create') }}" class="btn btn-primary">Tambah Data</a>
        </div>

        <style>
            .table {
                border: 2px solid black;
            }
    
            .table th, .table td {
                border: 2px solid black; 
            }
    
            .table thead th {
                border-bottom: 3px solid black;
            }
    
            .btn-primary {
                margin-left: 5px; 
            }
        </style>

        <table class="table table-bordered mt-3 text-center ">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nama Barang</th>
                    <th>Jumlah Barang</th>
                    <th>Tanggal Masuk</th>
                    <th>Actions</th>
                </tr>
                @foreach ($barangKeluar as $barang)
            <tbody>
                <tr>
                    <td>{{ $barang->id }}</td>
                    <td>{{ $barang->barang->nama_barang }}</td> 
                    <td>{{ $barang->jumlah }}</td>
                    <td>{{ $barang->tanggal_keluar }}</td>
                    <td>
                        <a href="{{ route('barangKeluar.edit', $barang->id) }}" title="Edit" class="btn btn-success">
                            <i class='bx bx-pencil'></i>
                        </a>
                        <form action="{{ route('barangKeluar.destroy', $barang->id) }}" method="POST" style="display:inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger" title="Hapus">
                                <i class='bx bx-trash'></i>
                            </button>
                        </form>
                    </td>
                </tr>
            </tbody>
            @endforeach
            </thead>
        </table>
    </div>
@endsection
