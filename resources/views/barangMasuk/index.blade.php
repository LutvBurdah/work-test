@extends('partial.template')
@section('content')
<style>
    .table {
        border: 2px solid black; 
    }

    .table th, .table td {
        border: 2px solid black; 
    }

    .table thead th {
        border-bottom: 3px solid black; 
    }

    .btn-primary {
        margin-left: 5px; 
    }
</style>
    <div class="table-responsive mt-5 p-5">
        <div class="">
            <a href="{{ route('barangMasuk.create') }}" class="btn btn-primary">
                <i class='bx bx-plus'></i> Tambah Data
            </a>
        </div>
        <table class="table table-bordered mt-3 text-center ">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nama Barang</th>
                    <th>Jumlah Barang</th>
                    <th>Tanggal Masuk</th>
                    <th>Actions</th>
                </tr>
                @foreach ($barangMasuk as $barang)
            <tbody>
                <tr>
                    <td>{{ $barang->id }}</td>
                    <td>{{ $barang->barang->nama_barang }}</td> 
                    <td>{{ $barang->jumlah }}</td>
                    <td>{{ $barang->tanggal_masuk }}</td>
                    <td>
                        <a href="{{ route('barangMasuk.edit', $barang->id) }}" title="Edit" class="btn btn-success">
                            <i class='bx bx-pencil'></i>
                        </a>
                        <form action="{{ route('barangMasuk.destroy', $barang->id) }}" method="POST" style="display:inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger" title="Hapus">
                                <i class='bx bx-trash'></i>
                            </button>
                        </form>
                    </td>
                </tr>
            </tbody>
            @endforeach
            </thead>
        </table>
    </div>
@endsection
