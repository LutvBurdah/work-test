@extends('partial.template')

@section('content')
<div class="container">
    <h2 class="mt5 p-5 text-center fw-bold">Tambah Barang Masuk</h2>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('barangMasuk.store') }}" method="POST" class="mt-5">
        @csrf
        <div class="input-group mt-3">
            <span class="input-group-text bg-secondary text-white">Nama Barang</span>
            <select name="barang_id" id="barang_id" class="form-select">
                @foreach($barang as $item)
                    <option value="{{ $item->id }}">{{ $item->nama_barang }}</option>
                @endforeach
            </select>
        </div>

        <div class="input-group mt-3">
            <span class="input-group-text bg-secondary text-white">jumlah</span>
            <input type="number" name="jumlah" class="form-control" id="jumlah" autocomplete="off" required>
        </div>

        <div class="input-group mt-3">
            <span class="input-group-text bg-secondary text-white">Tanggal Masuk</span>
            <input type="date" name="tanggal_masuk" class="form-control" id="tanggal_masuk" autocomplete="off" required>
        </div>

        <button type="submit" class="btn btn-primary mt-2">Simpan</button>
        <a href="{{ route('barangMasuk.index') }}" class="mt-2 btn btn-warning">Kembali</a>
    </form>
</div>
@endsection
