@extends('partial.template')
@section('content')
    <div class="table-responsive mb-3 p-5">
        <div class="">
            <H1 class="text-center fw-bold mt-3">Barang </H1>
            <a href="{{ route('barang.create') }}" class="btn btn-primary">Tambah Data</a>
        </div>

        <style>
            .table {
                border: 2px solid black;
            }
    
            .table th, .table td {
                border: 2px solid black; 
            }
    
            .table thead th {
                border-bottom: 3px solid black;
            }
    
            .btn-primary {
                margin-left: 5px; 
            }
        </style>

        <table class="table table-bordered mt-3 text-center">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nama Barang</th>
                    <th>Harga Barang</th>
                    <th>Stock</th>
                    <th>Kategori</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($barang as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->nama_barang }}</td>
                    <td>Rp. {{ number_format($item->harga_barang) }}</td>
                    <td>{{ $item->stock }}</td>
                    <td>{{ $item->category->category_name }}</td>
                    <td>
                        <a href="{{ route('barang.edit', $item->id) }}"title="edit" class="btn btn-success">
                            <i class='bx bx-pencil'></i>
                        </a>
                        <form action="{{ route('barang.destroy', $item->id) }}" method="POST" style="display:inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger" title="hapus">
                                <i class='bx bx-trash'></i>
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
