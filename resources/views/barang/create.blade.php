@extends('partial.template')
@section('content')
    <div class="container">
        <h1 class="text-center fw-bold mt-3">Tambah Barang</h1>
        <form method="POST" action="{{ route('barang.store') }}" class="p-4">
            @csrf
            <div class="input-group input-group-lg mt-3">
                <span class="input-group-text bg-secondary text-white">Nama Barang</span>
                <input type="text" name="nama_barang" class="form-control" autocomplete="off"/>
            </div>
            <div class="input-group input-group-lg mt-3">
                <span class="input-group-text bg-secondary text-white">Harga Barang</span>
                <input type="number" name="harga_barang" class="form-control" autocomplete="off"/>
            </div>
            <div class="input-group input-group-lg mt-3">
                <span class="input-group-text bg-secondary text-white">Stock</span>
                <input type="number" name="stock" class="form-control" autocomplete="off"/>
            </div>
            <div class="input-group input-group-lg mt-3">
                <span class="input-group-text bg-secondary text-white">Kategori</span>
                <select name="category_id" class="form-select">
                    @foreach($category as $kat)
                        <option value="{{ $kat->id }}">{{ $kat->category_name }}</option>
                    @endforeach
                </select>
            </div>
            <button class="btn btn-primary mt-2" type="submit">Save</button>
            <a href="{{ route('barang.index') }}" class="btn btn-warning mt-2">Cancel</a>
        </form>
    </div>
@endsection
