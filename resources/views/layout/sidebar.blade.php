<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo">
      <a href="index.html" class="app-brand-link">
        <span class="app-brand-text demo menu-text fw-bolder ms-2">Invensi Digital</span>
      </a>

      <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
        <i class="bx bx-chevron-left bx-sm align-middle"></i>
      </a>
    </div>

    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py-1">
      <!-- Dashboard -->
      <li class="menu-item active">
        <a href="index.html" class="menu-link">
          <i class="menu-icon tf-icons bx bx-home-circle"></i>
          <div data-i18n="Analytics">Dashboard</div>
        </a>
      </li>


      <li class="menu-header small text-uppercase">
        <span class="menu-header-text">Pages</span>
      </li>
      <li class="menu-item">
        <a href="javascript:void(0);" class="menu-link menu-toggle">
          <i class="menu-icon tf-icons bx bx-book"></i>
          <div data-i18n="Account Settings">Table</div>
        </a>
        <ul class="menu-sub">
          <li class="menu-item">
            <a href="/category" class="menu-link">
              <div data-i18n="Account">Category</div>
            </a>
          </li>
          <li class="menu-item">
            <a href="/barang" class="menu-link">
              <div data-i18n="Account">Barang</div>
            </a>
          </li>
          <li class="menu-item">
            <a href="/barangKeluar" class="menu-link">
              <div data-i18n="Account">Barang Keluar</div>
            </a>
          </li>
          <li class="menu-item">
            <a href="/barangMasuk" class="menu-link">
              <div data-i18n="Notifications">Barang Masuk</div>
            </a>
          </li>
          <li class="menu-item">
            <a href="/transaksi" class="menu-link">
              <div data-i18n="Connections">Transaksi</div>
            </a>
          </li>
        </ul>
      </li>
      <!-- Components -->
      <li class="menu-header small text-uppercase"><span class="menu-header-text">Laporan</span></li>
      <!-- Cards -->
      <li class="menu-item">
        <a href="/laporan/transaksi" class="menu-link">
          <i class="menu-icon tf-icons bx bx-collection"></i>
          <div data-i18n="Basic">Laporan Transaksi</div>
        </a>
      </li>
      <!-- User interface -->
    </ul>
  </aside>

  <script type="text/javascript">
    // Function to print the page
    function cetakHalaman() {
      window.print();
    }
  </script>
  
  <!-- Tombol cetak laporan -->
  <button type="button" onclick="cetakHalaman()" class="btn btn-primary">Cetak Laporan</button>