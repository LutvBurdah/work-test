@extends('partial.template')

@section('content')
    <div class="container mt-5">
        <div class="justify-content-between mb-3">
            <h1 class="text-center fw-bold">Category List</h1>
        </div>
        <a href="{{ route('category.create') }}" class="btn btn-primary mb-3">
            <i class='bx bx-plus'></i> Tambah Category
        </a>

        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif

        <style>
            .table {
                border: 2px solid black; /* Mengatur tebal garis tabel */
            }

            .table th, .table td {
                border: 2px solid black; /* Mengatur tebal garis sel tabel */
            }

            .table thead th {
                border-bottom: 3px solid black; /* Mengatur tebal garis bawah header tabel */
            }

            .btn-primary {
                margin-left: 5px; /* Menggeser sedikit ke kanan */
            }
        </style>
        
        <div class="container">
            <table class="table text-center">
                <thead>
                    <tr class="text-bold text-back">
                        <th>ID</th>
                        <th>Nama Kategori</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($category as $category)
                    <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->category_name }}</td>
                        <td>
                            <form action="{{ route('category.destroy', $category->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger"><i class='bx bx-trash'></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
