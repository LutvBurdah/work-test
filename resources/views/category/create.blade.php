@extends('partial.template')
@section('content')
<div class="container mt-5">
    <h1 class="text-center fw-bold mt-3">Add New Category</h1>
    <form method="POST" action="{{ route('category.store') }}" class="p-4">
        @csrf
        <div class="input-group input-group-lg mt-3">
            <span class="input-group-text bg-secondary text-white">Category Name</span>
            <input type="text" name="category_name" class="form-control" autocomplete="off" required />
        </div>
        <button class="btn btn-primary mt-2" type="submit">Save</button>
        <a href="{{ route('category.index') }}" class="btn btn-warning mt-2">Cancel</a>
    </form>
</div>
@endsection
